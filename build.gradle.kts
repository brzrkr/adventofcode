import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.61"
}

// Change these
group = "io.gitlab.berzi"
version = "0.0"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.5.2")
}


configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_12
}


tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "12"
}

tasks.test {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}