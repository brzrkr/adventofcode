package year2019.day2

import java.io.File


fun String?.parseInts(delimiters: String = ",", radix: Int = 10): List<Int> =
    this?.split(delimiters)?.mapNotNull { it.toIntOrNull(radix) }.orEmpty()


object Day2a {
    fun parseProgram(input: String, setup: Boolean = false): MutableList<Int> {
        val program = input.parseInts().toMutableList()

        // Additional setup: replace some initial values
        if (setup) {
            program[1] = 12
            program[2] = 2
        }

        fun MutableList<Int>.compute(pos: Int, operation: (Int, Int) -> Int) =
            run { this[this[pos + 3]] = operation(this[this[pos + 1]], this[this[pos + 2]]) }

        parse@ for (pos in program.indices) {
            if (pos == 0 || pos % 4 == 0) {
                when (program[pos]) {
                    99 -> break@parse
                    1  -> program.compute(pos, Int::plus)
                    2  -> program.compute(pos, Int::times)
                }
            }
        }

        return program
    }
}

fun main(args: Array<String>) {
    val filePath = args.firstOrNull() ?: return
    val firstValue = Day2a.parseProgram(File(filePath).readText().trim(), true).first()

    println(firstValue)
}
