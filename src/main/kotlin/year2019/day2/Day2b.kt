package year2019.day2

import java.io.File


class Day2b(private val input: String) {
    private fun parseProgram(noun: Int? = null, verb: Int? = null): MutableList<Int> {
        val program = input.parseInts().toMutableList()

        noun?.run { program[1] = this }
        verb?.run { program[2] = this }

        fun MutableList<Int>.compute(pos: Int, operation: (Int, Int) -> Int) =
            run { this[this[pos + 3]] = operation(this[this[pos + 1]], this[this[pos + 2]]) }

        parse@ for (pos in program.indices) {
            if (pos == 0 || pos % 4 == 0) {
                when (program[pos]) {
                    99 -> break@parse
                    1  -> program.compute(pos, Int::plus)
                    2  -> program.compute(pos, Int::times)
                }
            }
        }

        return program
    }

    fun bruteForce(desiredResult: Int): List<Int>? {
        for (verb in 0..99) {
            for (noun in 0..99) {
                if (parseProgram(noun, verb).first() == desiredResult) return listOf(noun, verb)
            }
        }

        return null
    }
}

fun main(args: Array<String>) {
    val input = args.firstOrNull()?.run { File(this).readText() } ?: return
    val desiredResult = args.getOrNull(1)?.toIntOrNull() ?: return

    val values = Day2b(input).bruteForce(desiredResult)

    println(values)
}
