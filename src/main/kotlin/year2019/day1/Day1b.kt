package year2019.day1

import java.io.File


object Day1b {
    fun calculateFuel(mass: Int, total: Int = 0): Int {
        val fuel = (mass / 3 - 2).coerceAtLeast(0)
        val newTotal = total + fuel
        return if (fuel == 0) {
            newTotal
        } else {
            calculateFuel(fuel, newTotal)
        }
    }
}


fun main(args: Array<String>) {
    val filePath = args.firstOrNull() ?: return
    val sum = File(filePath).readLines().map { Day1b.calculateFuel(it.toInt()) }.sum()

    println(sum)
}
