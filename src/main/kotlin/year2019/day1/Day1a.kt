package year2019.day1


import java.io.File


object Day1a {
    fun calculateFuel(mass: Int): Int = mass / 3 - 2
}


fun main(args: Array<String>) {
    val filePath = args.firstOrNull() ?: return
    val sum = File(filePath).readLines().map { Day1a.calculateFuel(it.toInt()) }.sum()

    println(sum)
}
