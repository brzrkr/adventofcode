package year2019.day4


object Day4a {
    fun possibilities(min: Int, max: Int): Int {
        val candidates = (min..max).filter {
            val asString = it.toString()
            asString.toSet().size <= asString.length
            && asString.zipWithNext().any {
                pair -> pair.first.toInt() == pair.second.toInt()
            }
            && asString.zipWithNext().all {
                pair -> pair.first.toInt() <= pair.second.toInt()
            }
        }

        return candidates.size
    }
}


fun main(args: Array<String>) {
    val (min, max) = args.map { it.toInt() }

    println(Day4a.possibilities(min, max))
}
