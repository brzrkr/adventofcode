package year2019.day5

import java.io.File


fun String?.parseInts(delimiters: String = ",", radix: Int = 10): List<Int> =
    this?.split(delimiters)?.mapNotNull { it.toIntOrNull(radix) }.orEmpty()


object Day5a {
    fun parseProgram(input: String, inputValue: Int): MutableList<Int> {
        val program = input.parseInts().toMutableList()

        fun convertParams(program: MutableList<Int>, params: List<Int>, modes: List<Int>): List<Int> =
            params.zip(modes).map { if (it.second == 1) { program[it.first] } else { program[program[it.first]] } }

        var skip = 0
        parse@ for (pos in program.indices) {
            skip.minus(1).coerceAtLeast(0)
            if (skip != 0) continue@parse

            val stringOpcode = program[pos].toString()
            val op = stringOpcode.takeLast(2).toInt()
            val modes = stringOpcode.dropLast(2).reversed().padStart(3, '0').toList().map { it.toInt() }

            when (op) {
                99 -> break@parse
                 1 -> program.run {
                     val args = convertParams(this, listOf(pos + 3, pos + 1, pos + 2), modes)
                     this[this[args[0]]] = args[1].plus(args[2])
                     skip = 3
                 }
                 2 -> program.run {
                     val args = convertParams(this, listOf(pos + 3, pos + 1, pos + 2), modes)
                     this[this[args[0]]] = args[1].times(args[2])
                     skip = 3
                 }
                 3 -> program.run {
                     this[this[pos + 1]] = inputValue
                     skip = 1
                 }
                 4 -> program.run {
                     val args = convertParams(this, listOf(pos + 1), modes)
                     println(this[this[args[0]]])
                     skip + 1
                 }
            }
        }
        return program
    }
}

fun main(args: Array<String>) {
    val filePath = args.firstOrNull() ?: return
    val inputValue = args.elementAtOrNull(1)?.toInt() ?: return

    Day5a.parseProgram(File(filePath).readText().trim(), inputValue)
}
