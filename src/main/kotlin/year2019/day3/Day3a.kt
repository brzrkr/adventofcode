package year2019.day3

import java.io.File
import kotlin.math.abs


data class Point(var x: Int, var y: Int) {
    fun distance(): Int = abs(x) + abs(y)
}


object Day3a {
    fun taxicabMetric(wires: List<String>): Int {
        val wiresSteps = wires.map { it.split(",") }
        val largestWireSize = wiresSteps.map { it.size }.max() ?: 0

        val wirePosition = MutableList(wires.size) { Point(0, 0) }
        val wirePath = List(wires.size) { mutableListOf<Point>() }

        val intersections = mutableListOf<Point>()

        fun move(wireIndex: Int, instructionIndex: Int) {
            val instructionString = wiresSteps[wireIndex].getOrElse(instructionIndex){ return }

            val instruction = instructionString[0] to instructionString.drop(1).toInt()
            val thisPoint = wirePosition[wireIndex]

            when (instruction.first) {
                'R' -> for (newPos in thisPoint.x+1..thisPoint.x+instruction.second) {wirePath[wireIndex].add(Point(newPos, thisPoint.y))}
                'L' -> for (newPos in thisPoint.x-1 downTo thisPoint.x-instruction.second) {wirePath[wireIndex].add(Point(newPos, thisPoint.y))}
                'U' -> for (newPos in thisPoint.y+1..thisPoint.y+instruction.second) {wirePath[wireIndex].add(Point(thisPoint.x, newPos))}
                'D' -> for (newPos in thisPoint.y-1 downTo thisPoint.y-instruction.second) {wirePath[wireIndex].add(Point(thisPoint.x, newPos))}
            }
            wirePosition[wireIndex] = wirePath[wireIndex].last()

            for (otherWireIndex in (0..wires.lastIndex).filter { it != wireIndex }) {
                intersections.addAll(wirePath[otherWireIndex].intersect(wirePath[wireIndex]))
            }
        }

        for (instructionIndex in 0..largestWireSize) {
            for (wireIndex in 0..wires.lastIndex) {
                move(wireIndex, instructionIndex)
            }
        }

        return intersections.map { it.distance() }.min() ?: 0
    }
}

fun main(args: Array<String>) {
    val input = args.firstOrNull()?.run { File(this).readText().trim() } ?: return

    val wires = input.split("\n")
    val value = Day3a.taxicabMetric(wires)

    println(value)
}
