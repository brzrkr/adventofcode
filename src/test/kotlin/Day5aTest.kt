import year2019.day5.Day5a.parseProgram
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import year2019.day5.parseInts


class Day5aTest {
    @Test
    fun `Test simple addition`() {
        assertEquals("2,0,0,0,99".parseInts(), parseProgram("1,0,0,0,99", 0))
    }

    @Test
    fun `Test simple multiplication and position`() {
        assertEquals("2,3,0,6,99".parseInts(), parseProgram("2,3,0,3,99", 0))
    }

    @Test
    fun `Test consecutive operation`() {
        assertEquals("30,1,1,4,2,5,6,0,99".parseInts(), parseProgram("1,1,1,4,99,5,6,0,99", 0))
    }
}
