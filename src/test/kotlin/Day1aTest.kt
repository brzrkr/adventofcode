import year2019.day1.Day1a.calculateFuel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class Day1aTest {
    @Test
    fun `Test with weight 12`() {
        assertEquals(2, calculateFuel(12))
    }

    @Test
    fun `Test with weight 14`() {
        assertEquals(2, calculateFuel(14))
    }

    @Test
    fun `Test with weight 1969`() {
        assertEquals(654, calculateFuel(1969))
    }

    @Test
    fun `Test with weight 100756`() {
        assertEquals(33583, calculateFuel(100756))
    }
}
