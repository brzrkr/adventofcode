import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import year2019.day3.Day3a.taxicabMetric


class Day3aTest {
    @Test
    fun `Test 2 short cables`() {
        val wires = listOf("R8,U5,L5,D3", "U7,R6,D4,L4")
        assertEquals(6, taxicabMetric(wires))
    }

    @Test
    fun `Test 2 long cables 1`() {
        val wires = listOf("R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83")
        assertEquals(159, taxicabMetric(wires))
    }

    @Test
    fun `Test 2 long cables 2`() {
        val wires = listOf("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7")
        assertEquals(135, taxicabMetric(wires))
    }
}
