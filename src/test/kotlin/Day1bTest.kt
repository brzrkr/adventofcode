import year2019.day1.Day1b.calculateFuel
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test


class Day1bTest {
    @Test
    fun `Test with weight 14b`() {
        assertEquals(2, calculateFuel(12))
    }

    @Test
    fun `Test with weight 1969b`() {
        assertEquals(966, calculateFuel(1969))
    }

    @Test
    fun `Test with weight 100756b`() {
        assertEquals(50346, calculateFuel(100756))
    }
}
